# Permadeath

## Requirements
OpenMW 0.49+

## Description
You only live once. All save files are constantly being deleted from this character. Saves are only generated when the main menu is activated. Therefore, please heed this **WARNING**: Exiting the game without using the "Exit" button in the main menu will cause a permanent loss of your character.

This could be handled more gracefully if circumventing player input for Quickload/Quicksave could be implemented. 
