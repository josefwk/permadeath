local I = require('openmw.interfaces')
local self = require('openmw.self')
local types = require('openmw.types')
local time = require('openmw_aux.time')
local input = require('openmw.input')

local mainMenuActivated = false
local hasDied = false

local function requestSave()
    local playerName = types.NPC.record(self.object.recordId).name
    types.Player.sendMenuEvent(self.object, "onCreateSaveRequest", playerName)
end

local function requestDeleteAllSaves()
    types.Player.sendMenuEvent(self.object, "onDeleteSavesRequest")
end

local function doHealthCheck()
    health = types.Actor.stats.dynamic.health(self).current
    local isDying = (health <= 0 and hasDied == false)

    if (isDying) then
        requestDeleteAllSaves()
        hasDied = true
        return
    end
end

local function doMainMenuActivationCheck()
    if (I.UI.getMode() ~= "MainMenu") then 
        mainMenuActivated = false
        return 
    else
        -- currently in main menu
        if (mainMenuActivated == true) then return end

        requestSave()
        mainMenuActivated = true
    end
end

local function getPlayerStatus()
    if (hasDied) then return end

    doHealthCheck()
    doMainMenuActivationCheck()
end

-- in lieu of preventing Quickload/Quicksave; would be better to circumvent those inputs altogether
time.runRepeatedly(requestDeleteAllSaves, 1 * time.second, { type = time.SimulationTime })

return {
    engineHandlers = {
        onFrame = getPlayerStatus
    }
}