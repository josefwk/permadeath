local menu = require('openmw.menu')

local function deleteAllPlayerSaves() 

    local directory = menu.getCurrentSaveDir()
    if (directory == nil) then return end

    local saves = menu.getSaves(directory)
    for i, s in pairs(saves) do
        menu.deleteGame(directory, i)
    end
    
end

local function saveGameAsync(playerName)
    deleteAllPlayerSaves()

    local name = playerName or ""
    local saveTitle = playerName .. " (Permadeath Mode)"

    menu.saveGame(saveTitle, "You only live once.")
end

return {
    eventHandlers = {
        onDeleteSavesRequest = deleteAllPlayerSaves,
        onCreateSaveRequest = saveGameAsync
    }
}